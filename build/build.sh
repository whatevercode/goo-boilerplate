r.js -o app.build.js
cd ../dist
rm -rf scripts/apps
rm -rf build
rm -rf Readme
rm -rf scripts/lib
rm -rf scripts/goo.js
find . -name "index.htm" -print0 | xargs -0 sed -i '' -e 's/scripts\/lib\/goo-require.js/scripts\/main.js/g'

