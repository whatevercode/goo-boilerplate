'use strict';
define([
    'base',
    'goo/renderer/Material',
    'goo/renderer/shaders/ShaderLib',
    'goo/entities/EntityUtils',
    'goo/math/Vector3',
    'goo/shapes/ShapeCreator'
], function (base, Material, ShaderLib, EntityUtils, Vector3, ShapeCreator) {

        //Create our namespace
        var ns = {};

        ns.addCube = function () {
            //init code here

            var goo = base.gooDOM;


            var meshData = ShapeCreator.createBox(50, 50, 50);
            // Create entity with meshdata and common components setup
            var entity = EntityUtils.createTypicalEntity(goo.world, meshData);
            var material = Material.createMaterial(ShaderLib.simpleLit);
            material.uniforms.materialAmbient = [0, 0.4, 0, 1];
            material.uniforms.materialDiffuse = [0, 0.7, 0, 1];
            entity.meshRendererComponent.materials.push(material);
            entity.transformComponent.transform.translation.x = -100;
            entity.addToWorld();

        };
        return ns;
    });