'use strict';
define([
    'base',
    'cube',
    'goo/renderer/Camera',
    'goo/renderer/Material',
    'goo/renderer/shaders/ShaderLib',
    'goo/renderer/light/PointLight',
    'goo/entities/components/CameraComponent',
    'goo/entities/components/ScriptComponent',
    'goo/entities/EntityUtils',
    'goo/entities/components/LightComponent',
    'goo/scripts/OrbitCamControlScript',
    'goo/math/Vector3',
    'goo/shapes/ShapeCreator'
], function (base, cube, Camera, Material, ShaderLib, PointLight, CameraComponent, ScriptComponent, EntityUtils, LightComponent, OrbitCamControlScript, Vector3, ShapeCreator) {

    //Create our namespace
    var ns = {};

    ns.init = function () {
        //init code here

        var goo = base.gooDOM;

        var camera = new Camera(45, 1, 0.1, 1000);
        var cameraEntity = goo.world.createEntity('CameraEntity');
        var cameraComponent = new CameraComponent(camera);

        cameraEntity.setComponent(cameraComponent);

        var camScript = new OrbitCamControlScript({
            domElement: goo.renderer.domElement,
            spherical: new Vector3(250, 0, 0),
            lookAtPoint: new Vector3(0, 0, 0),
            worldUpVector: Vector3.UNIT_Y,
            zoomSpeed: 2,
            maxZoomDistance: 500
        });

        cameraEntity.setComponent(new ScriptComponent(camScript));
        cameraEntity.addToWorld();

        var meshData = ShapeCreator.createSphere(12, 12, 12);
        // Create entity with meshdata and common components setup

        var entity = EntityUtils.createTypicalEntity(goo.world, meshData);
        var material = Material.createMaterial(ShaderLib.simpleLit);
        material.uniforms.materialAmbient = [0, 0, 0.4, 1];
        material.uniforms.materialDiffuse = [0, 0, 0.7, 1];
        entity.meshRendererComponent.materials.push(material);
        entity.addToWorld();

        var lightEntity = goo.world.createEntity('MyLight');
        lightEntity.setComponent(new LightComponent(new PointLight()));
        lightEntity.transformComponent.transform.translation.setd(0, 100, 100);
        lightEntity.addToWorld();

        //Adding example object
        cube.addCube();
    };
    return ns;
});