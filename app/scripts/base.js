'use strict';
define(['goo', 'goo/entities/GooRunner'], function (goo, GooRunner) {
    var ns = {};

    ns.gooDOM = new GooRunner({
        alpha: false,
        premultipliedAlpha: true,
        antialias: true,
        stencil: false,
        preserveDrawingBuffer: false, //activate this to enable "screenshots"
        showStats: true, //Activate to show fps and graph
        manuallyStartGameLoop: false,
        logo: false,
        tpfSmoothingCount: 10
    });
    ns.render = ns.gooDOM.renderer.domElement;
    ns.render.id = 'goo';
    document.body.appendChild(ns.render);
    return ns;
});