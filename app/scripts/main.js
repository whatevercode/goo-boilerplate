require.config({
    paths: {
//        jquery: '../bower_components/jquery/jquery',
        goo: '../bower_components/goo-require.js/index'
    }
});

require(['app'], function (app ) {
    'use strict';
    // use app here
    app.init();
});
